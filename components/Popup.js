export default function Popup(props) {
  return (props.trigger) ? (
      <div className="popup">
        <div className="popup-inner">
        {props.children}
          {/* <button className="pop-close-btn" onClick={()=> props.setTrigger(false)}>close</button> */}
        </div>
      </div>
  ) : (
    ""
  );
}
