import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";

export default function Navbar(){
  const [isactive, setIsactive] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [courses, setCourses] = useState(null)
  const [services, setServices] = useState(null)
  const [navbar, setNavbar] = useState(false);
  const [isOpenEdu, setOpenEdu] = useState(false);
  const [isOpenExp, setOpenExp] = useState(false);

  const toggleEdu = () => setOpenEdu(!isOpenEdu);
  const toggleExp = () => setOpenExp(!isOpenExp);

  const handleIsActive = () => {
    setIsactive(!isactive);
  };

  const changeBackground = () => {
    if (window.scrollY >= 60) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  };

  if (typeof window !== "undefined") {
    window.addEventListener("scroll", changeBackground);
  }

  useEffect(() => {
    setLoading(true)
  fetch("https://edulandimmigration.com/graphql", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `
        query Courses {
           courses {
             nodes {
               title
               slug
             }
           }
        }`,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCourses(data.data.courses)
        setLoading(false)
      })
  }, [setCourses])

  useEffect(() => {
    setLoading(true)
  fetch("https://edulandimmigration.com/graphql", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `
        query Services {
          services {
            nodes {
              title
              slug
            }
          }
        }`,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setServices(data.data.services)
        setLoading(false)
      })
  }, [setServices])

  const facilities = [
    {
      name: "Admission Advise & Assistance",
      slug: "/",
    },
    {
      name: "Financial & Forex Assistance",
      slug: "/",
    },
    {
      name: "Visa Advise & Assistance",
      slug: "/",
    },
    {
      name: "Bursaries and scholarship guidance",
      slug: "/",
    },
    {
      name: "Pre Departure Support",
      slug: "/",
    },
  ];

  return (
    <>
      <section className="header">
        <div className="header_top">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-sm-12 header_top_left">
                <div>
                  <Link href="https://goo.gl/maps/NoBYzgdrcVKHu9nJ9">
                    <a target="_blank" rel="noopener noreferrer">
                      <Image width={20} height={20} src="/home.svg" alt="Icon" />
                      First Floor, SCO 2455-2456, Sector 22 C, Chandigarh,
                      160022
                    </a>
                  </Link>
                </div>
                <div>
                  <Link href="mailto:contact@edulandimmigration.com">
                    <a target="_blank" rel="noopener noreferrer">
                      <Image width={20} height={15} src="/mail.svg" alt="Icon" />
                      contact@edulandimmigration.com
                    </a>
                  </Link>
                </div>
              </div>

              <div className="col-md-2 col-sm-12 header_top_right">
                <Link href="https://www.facebook.com/edulandimmigration">
                  <a target="_blank" rel="noopener noreferrer">
                    <Image width={18} height={18} src="/fb.svg" alt="Icon" />
                  </a>
                </Link>
                <Link href="https://www.instagram.com/edulandimmigration/">
                  <a target="_blank" rel="noopener noreferrer">
                    <Image width={18} height={18} src="/insta.svg" alt="Icon" />
                  </a>
                </Link>
                <Link href="https://twitter.com/Edulandofficial">
                  <a target="_blank" rel="noopener noreferrer">
                    <Image
                      width={18}
                      height={18}
                      src="/twitter.svg"
                      alt="Icon"
                    />
                  </a>
                </Link>
                <Link href="https://www.linkedin.com/in/edulandimmigration/">
                  <a target="_blank" rel="noopener noreferrer">
                    <Image
                      width={18}
                      height={18}
                      src="/linkedin.svg"
                      alt="Icon"
                    />
                  </a>
                </Link>

                <Link href="https://www.youtube.com/channel/UC1xB2xRU0B1i01JOygRTAmg">
                  <a target="_blank" rel="noopener noreferrer">
                    <Image
                      width={18}
                      height={18}
                      src="/youtube.svg"
                      alt="Icon"
                    />
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div
          className={navbar ? "header_bottom header_fixed" : "header_bottom"}
        >
          <div className="container">
            <div className="header_flex">
              <div className="logo">
                <Link href="/">
                  <a>
                    <Image width={80} height={90} src="/header-logo.svg" alt="Logo" />
                  </a>
                </Link>
              </div>
              <div className="header_right">
                <nav className="navbar navbar-expand-lg">
                  <div className="navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                      <li className="nav-item">
                        <Link href="/our-story">
                          <a className="nav-link">OUR STORY</a>
                        </Link>
                      </li>

                      <li className="nav-item dropdown">
                          <a
                            className="nav-link  dropdown-toggle"
                            data-bs-toggle="dropdown"
                          >
                            OVERSEAS EDUCATION
                          </a>
                        
                         {isLoading ? (<p>Loading...</p>) : (
                          <ul className="dropdown-menu">
                            {courses?.nodes.map((course) => (
                              <li key={course.slug}>
                                <Link className="dropdown-item" href={`/courses/${course.slug}`}>
                                  {course.title}
                                </Link>
                              </li>
                            ))}
                        </ul>
                         )}
                          
                      </li>
                      <li className="nav-item dropdown">
                          <a
                            className="nav-link  dropdown-toggle"
                            data-bs-toggle="dropdown"
                          >
                            OVERSEAS EXPOUSER
                          </a>
                          {isLoading ? (<p>Loading...</p>) : (
                          <ul className="dropdown-menu">
                            {services?.nodes.map((service) => (
                              <li key={service.slug}>
                                <Link className="dropdown-item" href={`/services/${service.slug}`}>
                                  {service.title}
                                </Link>
                              </li>
                            ))}
                        </ul>
                         )}
                      </li>
                      {/* <li className="nav-item dropdown">
                          <a
                            className="nav-link dropdown-toggle"
                            data-bs-toggle="dropdown"
                          >
                            FACILITIES
                          </a>
                        <ul className="dropdown-menu">
                          {facilities.map((item, index) => (
                            <li key={index}>
                              <Link className="dropdown-item" href={item.slug}>
                                {item.name}
                              </Link>
                            </li>
                          ))}
                        </ul>
                      </li> */}
                      <li className="nav-item">
                        <Link className="nav-link" href="/blogs">
                          BLOGS
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" href="/contact-us">
                          CONTACT US
                        </Link>
                      </li>
                    </ul>
                  </div>
                </nav>
                <div className="call_btn">
                    <a className="flex-center" href="tel:+91-99155-62155">
                      <div className="phone_icon">
                        <Image width={40} height={40} src="/phone.svg" alt="Icon" />
                      </div>
                      <div>
                      <p>Any Question? Call Us</p>
                      <h3>+91 99155-62155</h3>
                      </div>
                    </a>
                </div>
              </div>

              <button onClick={handleIsActive} className="header_toggle">
                <Image width={30} height={30} alt="logo" src="/toogle.svg" />
              </button>
            </div>
          </div>
        </div>
      </section>

      {isactive ? (
        <header className="header-trans">
          <div className="mobile-header">
            <div className="container">
              <div className="mobile-header-div">
                <div className="mobile_logo">
                  <Link className="navbar-brand" href="/">
                  <Image width={50} height={70}
                      src="/header-logo.svg"
                      alt="Logo"
                      className="img-fluid"
                    />
                  </Link>
                </div>
                <div>
                  <button className="navbar-cut-btn" onClick={handleIsActive}>
                  <Image width={25} height={25} alt="logo" src="/close.svg" />
                  </button>
                </div>
              </div>
              <div>
                <nav>
                  <div>
                    <div className="res-header-links">
                      <ul className="mobile-view-ul">

                        <li className="mobile-view-lists">
                          <Link href="/">
                            <a onClick={handleIsActive}>HOME</a>
                          </Link>
                        </li>

                        <li className="mobile-view-lists">
                          <Link href="/our-story">
                            <a
                              onClick={handleIsActive}
                              className="mobile-view-lists-clr"
                            >
                              OUR STORY
                            </a>
                          </Link>
                        </li>

                        <li className="react_dropdown">
                          <button
                            className="mobile-dropdown-header"
                            onClick={toggleEdu}
                          >
                            OVERSEAS EDUCATION
                          </button>
                          {isLoading ? (<p>Loading...</p>) : (
                          <ul className={`dropdown-body ${isOpenEdu && "open"}`}>
                            {courses?.nodes.map((course) => (
                              <li key={course.slug} className="dropdown-item">
                                <Link  className="mobile_dropitem" href={`/courses/${course.slug}`}>
                                  <a onClick={handleIsActive}>{course.title}</a>
                                </Link>
                              </li>
                            ))}
                        </ul>
                         )}
                        </li>
                        <li className="react_dropdown">
                          <button
                            className="mobile-dropdown-header"
                            onClick={toggleExp}
                          >
                            OVERSEAS EXPOUSER
                          </button>
                          {isLoading ? (<p>Loading...</p>) : (
                          <ul className={`dropdown-body ${isOpenExp && "open"}`}>
                            {services?.nodes.map((service) => (
                              <li key={service.slug} className="dropdown-item">
                                <Link  className="mobile_dropitem" href={`/services/${service.slug}`}>
                                  <a onClick={handleIsActive}>{service.title}</a>
                                </Link>
                              </li>
                            ))}
                        </ul>
                         )}
                        </li>
                        <li className="mobile-view-lists">
                          <Link href="/blogs">
                            <a onClick={handleIsActive}>BLOGS</a>
                          </Link>
                        </li>
                        <li className="mobile-view-lists">
                          <Link href="/contact-us">
                            <a onClick={handleIsActive}>CONTACT US</a>
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </header>
      ) : (
        ""
      )}
    </>
  )
}