import React, { useState, useEffect } from "react";
import Popup from "./Popup";
// import validator from "validator";
import Link from "next/link";
import Image from "next/image";
import Form from "./Form";

function Footer() {
  const [isLoading, setLoading] = useState(false);
  const [courses, setCourses] = useState(null);
  const [buttonPopup, setButtonpopup] = useState(false);


  const handleIsActive = () => {
    setButtonpopup(!buttonPopup);
  };

  useEffect(() => {
    setLoading(true);
    fetch("https://edulandimmigration.com/graphql", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `
        query Courses {
           courses {
             nodes {
               title
               slug
             }
           }
        }`,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCourses(data.data.courses);
        setLoading(false);
      });
  }, [setCourses]);

  return (
    <>
      <div className="popup_container">
        <button className="popup_btn" onClick={handleIsActive}>
          <i className={buttonPopup ? "bi bi-x" : "bi bi-chat-dots-fill"}></i>
        </button>

        <Popup trigger={buttonPopup} setTrigger={handleIsActive}>
          <div>
            <p className="popup_text">
              Please fill out the form below and we will get back to you as soon
              as possible.
            </p>

            <div className="message_form">
              <Form />
              {/* <form className="row form_contact">
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    name="name"
                    placeholder="You Name*"
                  />
                </div>
                <div className="form-group">
                  <input
                    type="email"
                    className="form-control"
                    name="email"
                    placeholder="Your Email*"
                  />
                </div>
                <div className="form-group col-md-12">
                  <textarea
                    name="message"
                    rows="4"
                    cols="50"
                    className="form-control"
                    placeholder="Your Message"
                  ></textarea>
                </div>
                <div className="form-group text-center">
                  <button type="submit" className="message_send">
                  <i className="bi bi-send-fill"></i>SUBMIT
                  </button>
                </div>
              </form> */}
            </div>
          </div>
        </Popup>
      </div>
      <section className="footer">
        <a
          target="_blank"
          rel="noopener noreferrer"
          className="common_btn common_btn2 payment_sticky"
          href="https://pages.razorpay.com/pl_JSFqFJNfvx5nt0/view"
        >
          Pay Online
        </a>

        <div className="sticky_bar">
          <a className="text-capitalize">
            <i className="bi bi-person-fill"></i>
          </a>
          <a href="mailto:contact@edulandimmigration.com">
            <i className="bi bi-envelope"></i>
          </a>
          <a href="https://goo.gl/maps/NoBYzgdrcVKHu9nJ9">
            <i className="bi bi-geo-alt"></i>
          </a>
          <a className="" href="tel:+91-99155-62155">
            <i className="bi bi-telephone"></i>
          </a>
          <a className="text-capitalize" data-toggle="modal">
            <i className="bi bi-file-earmark-text"></i>
          </a>
        </div>
        <div className="footer_upper w-100">
          <div className="container">
            <div className="row">
              <div className="col-lg-2 col-md-3 col-sm-12 footer_column text-center">
                <img src="/footer-logo.svg" alt="Logo" className="img-fluid" />
                <div className="social_links">
                  <Link
                    href="https://www.facebook.com/edulandimmigration"
                    target="_blank"
                  >
                    <a target="_blank" rel="noopener noreferrer">
                      <Image width={18} height={18} src="/fb.svg" alt="Icon" />
                    </a>
                  </Link>

                  <Link href="https://www.instagram.com/edulandimmigration/">
                    <a target="_blank" rel="noopener noreferrer">
                      <Image
                        width={18}
                        height={18}
                        src="/insta.svg"
                        alt="Icon"
                      />
                    </a>
                  </Link>
                  <Link href="https://twitter.com/Edulandofficial">
                    <a target="_blank" rel="noopener noreferrer">
                      <Image
                        width={18}
                        height={18}
                        src="/twitter.svg"
                        alt="Icon"
                      />
                    </a>
                  </Link>
                  <Link href="https://www.linkedin.com/in/edulandimmigration/">
                    <a target="_blank" rel="noopener noreferrer">
                      <Image
                        width={18}
                        height={18}
                        src="/linkedin.svg"
                        alt="Icon"
                      />
                    </a>
                  </Link>
                  <Link href="https://www.youtube.com/channel/UC1xB2xRU0B1i01JOygRTAmg">
                    <a target="_blank" rel="noopener noreferrer">
                      <Image
                        width={18}
                        height={18}
                        src="/youtube.svg"
                        alt="Icon"
                      />
                    </a>
                  </Link>
                </div>
              </div>
              <div className="col-lg-3 offset-lg-1 col-md-3 col-sm-12 footer_column">
                <h3>Coaching</h3>
                <ul>
                  {isLoading ? (
                    <p>Loading...</p>
                  ) : (
                    <>
                      {courses?.nodes.map((course) => (
                        <li key={course.slug}>
                          <Link href={`/courses/${course.slug}`}>
                            <a>{course.title.replace(/\w+[.!?]?$/, "")}</a>
                          </Link>
                        </li>
                      ))}
                    </>
                  )}
                </ul>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12 footer_column">
                <h3>Quick Links</h3>
                <ul>
                  <li>
                    <Link href="/our-story">
                      <a>Our Story</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/blogs">
                      <a>Blogs</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/contact-us">
                      <a>Contact Us</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/payments">
                      <a>Payments</a>
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12 footer_column">
                <h3>Reach Us</h3>
                <p>
                  First Floor, SCO 2455-2456,Sector 22 C, Chandigarh, 160022
                </p>
                <p>Mon-Fri: 9:30 am - 6:00 pm</p>
                <p>Sat: 9:30 am - 5:00 pm</p>
              </div>

              <div className="clearfix"></div>

              <div className="powered_by row w-100">
                <div className="col-lg-6 col-md-6 col-sm-12 powered_by_column text-left">
                  <div>
                    <Link href="/terms-and-conditions">
                      <a>Terms &amp; Conditions</a>
                    </Link>
                    <Link href="/privacy-policy">
                      <a>Privacy Policies</a>
                    </Link>
                  </div>
                </div>

                <div className="col-lg-6 col-md-6 col-sm-12 powered_by_column text-right">
                  <p>
                    Powered By:
                    <Link href="https://unmeteredtechnologies.com/">
                      <a target="_blank" rel="noopener noreferrer">
                        <img src="/unmetered-logo.gif" alt="Icon" />
                      </a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Footer;
