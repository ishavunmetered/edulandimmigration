import { useState, useEffect } from "react";

export default function Form() {
  const initialValues = {
    name: "",
    email: "",
    phone: "",
    city: "",
    message: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  };
  useEffect(() => {
    // console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      alert("Thanks For Contacting Us");
    }
  }, [formErrors, isSubmit]);

  const validate = (values) => {
    const errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    if (!values.name) {
      errors.name = "Name is required!";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!regex.test(values.email)) {
      errors.email = "This is not a valid email format!";
    }
    if (!values.phone) {
      errors.phone = "Phone is required!";
    }
    if (!values.city) {
      errors.city = "City is required!";
    }
    if (!values.message) {
      errors.message = "Message is required!";
    }
    return errors;
  };

  return (
    <>
      <div className="service_inquiry w-100">
        {Object.keys(formErrors).length === 0 && isSubmit ? (
          <div className="alert alert-success" role="alert">
            Thank You For Contacting us
          </div>
        ) : (
          console.log("")
          //   <div className="alert alert-danger" role="alert">
          //   Invalid Response
          // </div>
        )}
        <form className="row form_contact" onSubmit={handleSubmit}>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name="name"
              onChange={handleChange}
              value={formValues.name}
              placeholder="You Name*"
            />
            <p className="error-field">{formErrors.name}</p>
          </div>
          <div className="form-group">
            <input
              type="email"
              className="form-control"
              name="email"
              value={formValues.email}
              onChange={handleChange}
              placeholder="Your Email*"
            />
            <p className="error-field">{formErrors.email}</p>
          </div>
          <div className="form-group">
            <input
              type="number"
              className="form-control"
              name="phone"
              value={formValues.phone}
              onChange={handleChange}
              placeholder="Phone Number*"
            />
            <p className="error-field">{formErrors.phone}</p>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name="city"
              value={formValues.city}
              onChange={handleChange}
              placeholder="City You'r Living In*"
            />
            <p className="error-field">{formErrors.city}</p>
          </div>
          <div className="form-group col-md-12">
            <textarea
              name="message"
              rows="4"
              cols="50"
              value={formValues.message}
              onChange={handleChange}
              className="form-control"
              placeholder="Your Message"
            ></textarea>
            <p className="error-field">{formErrors.message}</p>
          </div>
          <div className="col-md-12 form-group mb-0 text-center">
            <button type="submit" className="common_btn common_btn2">
              SUBMIT
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
