
module.exports = {
  parser: "@babel/eslint-parser",
  env: {
    GRAPGQL_URL: process.env.GRAPHQL_URL,
  },
  reactStrictMode: true,
}
