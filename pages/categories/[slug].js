import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import Form from "../../components/Form";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";
import "react-accessible-accordion/dist/fancy-example.css";

function Category(data) {
  const category = data.category;

  const accordiionData = [
    {
      title:
        "What is the minimum amount of money required for Canada Student Visa ?",
      text: "Canada requires all international applicants to have one year of their respective college/university tuition fees paid along with one year of living cost to be paid before applying for Canada Student Visa approval so that student can focus on their studies while reaching Canada rather than on their work .  Canada is ranked 5th in QS world rankings for the Education System and offers masters,bachelors,graduate certificates,advanced diplomas ,diplomas and certicates to its international students.On an average,students needs to pay CAD 20000- CAD 30000 as their tuition fees and any miscellaneous charges depending on the university /college they are option to study.Ling cost for on eyear in Canada is 10,200 CAD which is paid by the student as GIC in the authorised banks  like Scotia Bank/ICICI Bank / SBI Canada /HSBC / Canadian Imperial Bank of Commerce which are approved by Canadian High Commission. Canada also offers scholarships and bursaries to international students which includes several factors and student student has to apply well in advance for the same. ",
    },
    {
      title: "What are post study work rights in Canada ?",
      text: "An international student pursuing a full time course at a DLI institution with a valid study permit having length of more than 6 months is eligible to work both on campus and off campus.During the studies,student can work 20 hours per week and can work for 40 hours per week during vacations.They can earn anywhere between 11 CAD to 18 CAD per hour and a lot of students cover their living expense and some amount of their tuition fees from their part time work.After completion of their studies,international students need have a valid work permit to work in Canada and have option of 1-3 year of stayback depending on the length of their program in Canada.",
    },
    {
      title: "Am I eligible for PR ?",
      text: "Studying and graduating from Canada brings lot of career opportunities for international applicants.Many applicants are interested to apply for permanent residency due to the career growth and good quality of lifein Canada and moreover governmet of Canada also retains foreign applicants who graduated from their institutions.Canada welcome applicants to apply for both temporary study and work visa and permanent residency both.International applicants can apply for permanent residency through below given options:  \n\n Provincial Nominee Program (PNP) : Each of Canada’s provinces and territories ,except Quebec and Nunavat,operate its own PNP program with different streams.For example,of you have a job offer in Ontario then you can apply PR through Ontario Immigrant Nominee Program. \n\n Canadian Experience Class (CEC) : Any international Student who has studied 2 year full time and have atleast one year of work experience in a full time skilled position may qualify for PR under this category.  \n\n Federal Skilled Worker Class : This program takes several factors apart from work ecperience such as age ,Language ability,skills etc for processing.",
    },
    {
      title: "What is OL,COL and LOA ?",
      text: "OL stands for offer letter which is an acceptance for international student who applied for his further studies in Canada.COL stands for Conditional offer letter and which is also a form of acceptance but with some conditions mentioned on the offer letter and student has to complete the conditions before moving forward.LOA stands for Letter of Acceptance which is obtained once the tuition fees is paid to the college.Some Canadian institutions offer LOA at the very fist stage and some offer LOA after the tuition fees is paid and it depends the type of college/university student is applying for.",
    },
    {
      title: "What is GIC ?",
      text: "GIC stands for Gauranteed Investment Certificate which offers a guaranteed investment over a fixed period of time.International students are required to open up GIC account and pay CAD 10200 as their living cost for one year which will be paid to them on monthly basis once they reach Canada.This GIC service can be taken from any one the banks like Scotia Bank/ICICI Bank / SBI Canada /HSBC / Canadian Imperial Bank of Commerce.",
    },
    {
      title: "What is education System in Canada",
      text: "Canada makes a large investment in its education system. In fact, Canada is one of the world’s top education performers and among the top three countries in spending per capita on public post-secondary education, according to the Organization for Economic Co-operation and Development (OECD). Canadian Education is generally divided into primary education, followed by secondary education and post-secondary. Within the provinces under the ministry of education, there are district school boards administering the educational programs. ",
    },
  ];

  const steps = [
    {
      head: "Step 1",
      subhead: "Know your visa type",
      image: "/step-icons/step-1.svg",
    },
    {
      head: "Step 2",
      subhead: "Complete your Application",
      image: "/step-icons/step-2.svg",
    },
    {
      head: "Step 3",
      subhead: "Pay your visa Fee",
      image: "/step-icons/step-3.svg",
    },
    {
      head: "Step 4",
      subhead: "Schedule your Appointment",
      image: "/step-icons/step-4.svg",
    },
    {
      head: "Step 5",
      subhead: "Attend your Interview",
      image: "/step-icons/step-5.svg",
    },
    {
      head: "Step 6",
      subhead: "Track your Passport",
      image: "/step-icons/step-6.svg",
    },
  ];

  return (
    <>
      <Head>
        <title>{category.name} || Eduland Immigration</title>
        <meta name="description" content="Learn more about Canadian Institute" />
        <meta
          property="og:title"
          content="Canadian Institute - Eduland Immigration"
        />

        <meta
          property="og:url"
          content={`https://edulandimmigration.com/categories/${category.slug}`}
        />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.png" />
      </Head>

      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/inner-banner/education/ielts-banner.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center g_medium">{category.name}</h1>
          </div>
        </div>
      </section>

      <section className="services_single_section  common_padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4 col-sm-12 service_content_left">
            <Form />
            </div>

            <div className="col-lg-9 col-md-8 col-sm-12 page_content service_content_right">
              <div className="services_content_image w-100">
                <img
                  src="/inner-banner/education/ielts-banner.jpg"
                  alt="Image"
                />
              </div>

              <div className="entry-content">
                <article
                  className="post_content"
                  dangerouslySetInnerHTML={{ __html: category.description }}
                ></article>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="blogs_section common_padding grey_bg">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 common_tittle text-center steps_head">
              <h2 className="text-capitalise g_medium">
                EI Overseas Education Process
              </h2>
              <span>
                <img src="/heading-line.svg" alt="Icon" />
              </span>
            </div>

            <div className="steps_section country_steps_section w-100">
              <div className="row no_margin">
                {steps.map((step) => (
                  <div className="col-lg-2 col-md-4 col-sm-12 steps_section_column text-center" key={step.head}>
                    <span>
                      <img
                        width={40}
                        height={50}
                        alt="Know your visa type"
                        src={step.image}
                      />
                    </span>
                    <h3>{step.head}</h3>
                    <p>{step.subhead}</p>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="blogs_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 common_tittle text-left">
              <p className="text-uppercase">HOW WE HELP CLIENTS</p>
              <h2 className="text-capitalise g_medium">
                EI Overseas Education Process
              </h2>
              <span>
                <Image
                  width={80}
                  height={40}
                  src="/heading-line.svg"
                  alt="Icon"
                />
              </span>
            </div>
            <div className="col-md-12 col-sm-12">
              <Accordion className="accordion_main">
                {accordiionData.map((accordion, index) => (
                  <AccordionItem className="accordion_card" key={index}>
                    <AccordionItemHeading>
                      <AccordionItemButton>
                        <h3 className="accordion_title g_medium">
                          {accordion.title}
                        </h3>
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                      <p>{accordion.text}</p>
                    </AccordionItemPanel>
                  </AccordionItem>
                ))}
              </Accordion>
            </div>
          </div>
        </div>
      </section>

      <section className="advisor_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-sm-12 advisor_section_column">
              <h2 className="g_medium">Got something else in your mind?</h2>
              <p>
                We’re here to answer! If you don’t see your question here, drop
                us a line on our Contact Page.
              </p>
            </div>
            <div className="col-md-4 col-sm-12 advisor_section_column">
              <div className="advisor_contact_btn">
                <Link href="/contact-us">Contact Us</Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps(context) {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: `
          query SingleCategory($id: ID!, $idType: CategoryIdType!) {
            category(id: $id, idType: $idType) {
              slug
              name
              description
            }
          }
              `,
      variables: {
        id: context.params.slug,
        idType: "SLUG",
      },
    }),
  });

  const json = await res.json();

  return {
    props: {
      category: json.data.category,
    },
  };
}

export async function getStaticPaths() {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
      query Categories {
        categories {
          nodes {
            name
            slug
          }
        }
      }`,
    }),
  });

  const json = await res.json();
  const categories = json.data.categories.nodes;

  const paths = categories.map((category) => ({
    params: { slug: category.slug },
  }));

  return {
    paths,
    fallback: false,
  };
}

export default Category;
