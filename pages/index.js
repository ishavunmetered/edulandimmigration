import React from "react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import styles from "../styles/Home.module.css";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function Home({ posts , services, categories }) {

  const serviceCarousel = {
    items: 3,
    margin: 20,
    dots: false,
    responsiveclassname: "true",
    responsive: {
      0: {
        items: 1,
        nav: true,
      },
      600: {
        items: 2,
        nav: true,
      },
      1000: {
        items: 3,
        loop: false,
        nav: true,
      },
    },
  };

  return (
    <>
      <Head>
        <title>Eduland Immigration</title>
        <meta
          name="description"
          content="Eduland Immigration Is A Chandigarh-Based Visa Consultancy Firm In India That Steps Forward To Facilitate Aspiring Students, Job Seekers, And Individuals Seeking A Visitor Visa."
        />
        <meta property="og:title" content="Eduland Immigration" />
        <meta
          property="og:description"
          content="Eduland Immigration Is A Chandigarh-Based Visa Consultancy Firm In India That Steps Forward To Facilitate Aspiring Students, Job Seekers, And Individuals Seeking A Visitor Visa."
        />
        <meta property="og:url" content="https://edulandimmigration.com/" />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.png" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      
      <section
        className="banner"
        style={{
          backgroundImage: 'url("/home-banner.jpg")',
        }}
      >
        <div className="banner_text">
          <div className="container">
            <div className="col-md-6 md-8 col-sm-12 common_title">
              <h1 className="g_medium">
                Our Simple Approach To Immigration Process
              </h1>
              <p className="text-capitalize ">
                Eduland immigration is a Chandigarh-based Visa consultancy firm
                in India that steps forward to facilitate aspiring students, job
                seekers, and individuals seeking a visitor Visa.
              </p>
              <Link href="/contact-us">
                <a className="common_btn">
                  Discover Solutions<i className="bi bi-arrow-right"></i>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </section>

      <section className="who_we_are common_padding pb-0">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-5 col-sm-12 who_we_are_left">
              <img src="/about-img.jpg" alt="Image" />
            </div>

            <div className="col-lg-6 offset-lg-1 col-md-7 col-sm-12 who_we_are_left who_we_are_right">
              <div className="common_tittle">
                <p className="text-uppercase">WHO we are?</p>
                <h2 className="text-capitalise g_medium">
                  15+ Years Of Your Trust And Recommendation
                </h2>
                <span>
                  <Image
                    width={60}
                    height={20}
                    src="/heading-line.svg"
                    alt="Icon"
                  />
                </span>
              </div>
              <p>
                Eduland immigration is a Chandigarh-based Visa consultancy firm
                in India that steps forward to facilitate aspiring students, job
                seekers, and individuals seeking a visitor Visa. Our insights
                are based on experience, knowledge, and data, and our
                organization is based on integrity and honesty. Eduland
                immigration is a registered and licensed firm owned and run by
                Mr. Gurpreet Singh Channa, who has a vast experience of over 15
                years in the consultancy world.
              </p>
              <div className="who_column">
                <span>
                  <img src="/staff.png" alt="Icon" />
                </span>
                <h4>Qualified Staff</h4>
                <p>Highly professional and well qualified team</p>
              </div>
              <div className="who_column">
                <span>
                  <img src="/institution.png" alt="Icon" />
                </span>
                <h4>10000+ Satisfied Clients</h4>
                <p>Most trusted &amp; recommended by thousands of clients.</p>
              </div>
              <Link href="/our-story">
                <a className="common_btn common_btn2">
                  Read More<i className="bi bi-arrow-right"></i>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </section>

      <section className="services_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 common_tittle text-center">
              <p className="text-uppercase">HOW WE HELP our CLIENTS</p>
              <h2 className="text-capitalise g_medium">
                Immigration &amp; Visa Services
              </h2>
              <span>
                <Image
                  width={60}
                  height={20}
                  src="/heading-line.svg"
                  alt="Icon"
                />
              </span>
            </div>

            <div className="col-md-12 service_slider_main">
              <OwlCarousel
                className="service-carousel owl-theme"
                responsiveClass="true"
                nav
                {...serviceCarousel}
              >
                {services.nodes.map((service) => (
                  <div
                    className="services_slider_column text-center"
                    key={service.slug}
                  >
                    <div className="services_slider_image">
                      <img
                        src={service.featuredImage.node.sourceUrl}
                        alt="Permanent Residency (PR)"
                      />
                    </div>
                    <span>
                      <Image
                        width={40}
                        height={40}
                        src="/service-icon/dependent.svg"
                        alt="icon"
                      />
                    </span>
                    <h3 className="common_color">{service.title}</h3>
                    <p>lorem ipsum ndfijdb sdbfuhabsduf asdbfuabsuf sdafbubuhysd fahus dfhuabsduf ahbdfhabf dhb</p>
                    <Link href={`/services/${service.slug}`}>
                      <a>
                        Read More<i className="bi bi-arrow-right"></i>
                      </a>
                    </Link>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </section>

      <section className="request_call_section">
        <div className="container">
          <div className="row no_margin">
            <div className="col-md-12">
              <div className="request_call_wrapper row">
                <div className="col-md-4 col-sm-12 request_call_wrapper_image text-center no_padding">
                  <img src="/call-back.jpg" alt="Image" />
                </div>
                <div className="col-md-8 col-sm-12 request_call_wrapper_column text-left">
                  <h4>Get Free Online Visa Assessment Today!</h4>
                  <h2 className="g_medium">
                    Top Rated By Customers With 100% Success Rate.
                  </h2>
                  <Link href="/contact-us">
                    <a className="common_btn">
                      Request Call Back<i className="bi bi-arrow-right"></i>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="countries_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 common_tittle text-center">
              <p className="text-uppercase">COUNTRIES WE OFFER SUPPORT</p>
              <h2 className="text-capitalise g_medium">
                Immigration &amp; Visa Services For Following Countries
              </h2>
              <span>
                <Image
                  width={60}
                  height={20}
                  src="/heading-line.svg"
                  alt="Icon"
                />
              </span>
            </div>

            <div className="col-md-12 service_slider_main">
              <OwlCarousel
                className="service-carousel owl-theme"
                responsiveClass="true"
                nav
                {...serviceCarousel}
              >
                {categories?.nodes.map((category) => (
                  <div
                    className="services_slider_column text-center"
                    key={category.slug}
                  >
                    <Link href={`/categories/${category.slug}`}>
                      <a>
                        <div className="services_slider_image">
                          <img src="/country-data/us.jpg" alt="Image" />
                        </div>
                        <div className="flag_country">
                          <div className="count_flag">
                            <Image
                              width={50}
                              height={50}
                              src="/country-data/us-flag.png"
                              alt="icon"
                            />
                          </div>
                          <h3 className="common_color">{category.name}</h3>
                        </div>
                      </a>
                    </Link>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </section>

      <section className="blogs_section common_padding grey_bg">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-sm-12 common_tittle text-left">
              <p className="text-uppercase">RECENT BLOG</p>
              <h2 className="text-capitalise g_medium">
                Recent Updates of Visa And Immigration
              </h2>
              <span>
                <Image
                  width={60}
                  height={20}
                  src="/heading-line.svg"
                  alt="Icon"
                />
              </span>
            </div>

            <div className="col-md-4 col-sm-12 blogs_section_header text-right">
              <Link href="/blogs">
                <a className="common_btn common_btn2">
                  Read All Blogs<i className="bi bi-arrow-right"></i>
                </a>
              </Link>
            </div>

            <div className="col-md-12 service_slider_main">
              <OwlCarousel
                className="service-carousel owl-theme"
                responsiveClass="true"
                nav
                {...serviceCarousel}
              >
                {posts.nodes.map((post) => (
                  <div
                    className="blog_slider_column text-center"
                    key={post.slug}
                  >
                    <Link href={`/blogs/${post.slug}`}>
                      <a>
                        <div className="blog_slider_image">
                          <img
                            src={post.featuredImage.node.sourceUrl}
                            alt={post.slug}
                          />
                        </div>
                        <div className="blog_slider_content  text-left">
                          <h4>{post.title}</h4>
                          <div className="date_time">
                            <Image
                              width={20}
                              height={20}
                              src="/clock.svg"
                              alt="Icon"
                            />
                            <span>{post.date}</span>
                          </div>
                          <div className="common_btn common_btn2">
                            Read More<i className="bi bi-arrow-right"></i>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps() {
  const blogsRes = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: `
      query AllPostsQuery {
        posts {
          nodes {
            slug
            title
            date
            featuredImage {
              node {
                sourceUrl
              }
            }
          }
        }
      }`,
    }),
  });

  const servicesRes = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
      query Services {
        services {
          nodes {
            title
            slug
            featuredImage {
              node {
                sourceUrl
              }
            }
          }
        }
      }`,
    }),
  });

  const categoriesRes = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
      query Categories {
        categories {
          nodes {
            name
            slug
          }
        }
      }`,
    }),
  });

  const blogsJson = await blogsRes.json();
  const servicesJson = await servicesRes.json();
  const categoriesJson = await categoriesRes.json();


  return {
    props: {
      posts: blogsJson.data.posts,
      services: servicesJson.data.services,
      categories: categoriesJson.data.categories,
    },
  };
}