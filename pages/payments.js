import Link from "next/link";
function Payments() {
  return (
    <>
      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/contact.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center">Payments</h1>
          </div>
        </div>
      </section>
      <section className="page_content common_padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12">
              <div className="payment_page_column w-100 text-center">
                <h3>
                  If you want to pay us by cash deposit or online transfer,
                  please proceed with following bank details :
                </h3>
                <table>
                  <tbody>
                    <tr>
                      <td>Bank :</td>
                      <td>ICICI Bank</td>
                    </tr>
                    <tr>
                      <td>Account Name :</td>
                      <td>Eduland Immigration</td>
                    </tr>
                    <tr>
                      <td>Account Number :</td>
                      <td>341505000825</td>
                    </tr>
                    <tr>
                      <td>Branch :</td>
                      <td>Sector-22, Chandigarh.</td>
                    </tr>
                    <tr>
                      <td>IFSC Code :</td>
                      <td>ICIC0003415</td>
                    </tr>
                  </tbody>
                </table>
                <h4>
                  If you want to pay online click on any of the payment methods
                  below.
                </h4>
                <Link href="https://pages.razorpay.com/pl_JSFqFJNfvx5nt0/view">
                  <a className="common_btn common_btn2">
                    Pay Online<i className="bi bi-arrow-right"></i>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="advisor_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-8 col-sm-12 advisor_section_column">
              <h2 className="g_medium">
                If you still have any query feel free to contact us
              </h2>
            </div>
            <div className="col-md-4 col-sm-12 advisor_section_column">
              <div className="advisor_contact_btn">
               <Link href="/contact-us"><a>Contact Us</a></Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Payments;
