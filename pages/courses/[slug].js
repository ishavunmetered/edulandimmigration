import Head from "next/head";
import Form from "../../components/Form";
function Course(data) {
  const course = data.course;

  return (
    <>
      <Head>
        <title>{course.title} || Eduland Immigration</title>
        <meta name="description" content={`Learn more about ${course.title}`} />
        <meta
          property="og:title"
          content={`${course.title} - Eduland Immigration`}
        />
    
        <meta
          property="og:url"
          content={`https://edulandimmigration.com/courses/${course.slug}`}
        />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/inner-banner/education/ielts-banner.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center">{course.title}</h1>
          </div>
        </div>
      </section>

      <section className="services_single_section  common_padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4 col-sm-12 service_content_left">
              <Form />
            </div>

            <div className="col-lg-9 col-md-8 col-sm-12 page_content service_content_right">
              <div className="services_content_image w-100">
                <img
                  src="/inner-banner/education/ielts-banner.jpg"
                  alt="Image"
                />
              </div>

              <div className="entry-content">
                <article
                  className="post_content"
                  dangerouslySetInnerHTML={{ __html: course.content }}
                ></article>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps(context) {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: `
            query SingleCourse($id: ID!, $idType: CourseIdType!) {
              course(id: $id, idType: $idType) {
                slug
                title
                content
              }
            }
              `,
      variables: {
        id: context.params.slug,
        idType: "SLUG",
      },
    }),
  });

  const json = await res.json();

  return {
    props: {
      course: json.data.course,
    },
  };
}

export async function getStaticPaths() {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
        query Courses {
          courses {
            nodes {
              slug
              title
              content
            }
          }
       }`,
    }),
  });

  const json = await res.json();
  const courses = json.data.courses.nodes;

  const paths = courses.map((course) => ({
    params: { slug: course.slug },
  }));

  return {
    paths,
    fallback: false,
  };
}

export default Course;
