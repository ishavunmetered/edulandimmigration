import Image from "next/image";
import Head from "next/head";

var $ = require("jquery");
if (typeof window !== "undefined") {
  window.$ = window.jQuery = require("jquery");
}

import dynamic from "next/dynamic";
const OwlCarousel = dynamic(() => import("react-owl-carousel"), {
  ssr: false,
});

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function OurStory() {
  const testimonial = {
    // center: true,
    items: 2,
    margin: 20,
    nav: false,
    autoplay: false,
    loop: true,
    dots: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveclassname: "true",
    responsive: {
      0: {
        items: 1,
        dots: true,
      },
      600: {
        items: 2,
        dots: true,
      },
      1000: {
        items: 2,
        dots: true,
        loop: true,
      },
    },
  };

  const testiData = [
    {
      name: "Damanpreet Kaur",
      title: "WILL RECOMMENDE FOR OThER",
      description:
        "My experience with eduland immigration has been incredible for applying tourist visa of canada as well  as we have applied daughters study visa from them.Whole team is supportive and responsible.",
    },
    {
      name: "Damanpreet Kaur",
      title: "WILL RECOMMENDE FOR OThER",
      description:
        "My experience with eduland immigration has been incredible for applying tourist visa of canada as well  as we have applied daughters study visa from them.Whole team is supportive and responsible.",
    },
    {
      name: "Damanpreet Kaur",
      title: "WILL RECOMMENDE FOR OThER",
      description:
        "My experience with eduland immigration has been incredible for applying tourist visa of canada as well  as we have applied daughters study visa from them.Whole team is supportive and responsible.",
    },
    {
      name: "Damanpreet Kaur",
      title: "WILL RECOMMENDE FOR OThER",
      description:
        "My experience with eduland immigration has been incredible for applying tourist visa of canada as well  as we have applied daughters study visa from them.Whole team is supportive and responsible.",
    },
  ];

  return (
    <>
      <Head>
        <title>Our Story - Eduland Immigration</title>
        <meta
          name="description"
          content="Being the leading immigration and student consultancy firm, we endeavor to ensure a bright future by opening the doors of opportunities to our valued clients. "
        />
        <meta property="og:title" content="Our Story - Eduland Immigration" />
        <meta
          property="og:description"
          content="Being the leading immigration and student consultancy firm, we endeavor to ensure a bright future by opening the doors of opportunities to our valued clients. "
        />
        <meta
          property="og:url"
          content="https://edulandimmigration.com/our-story"
        />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.png" />
      </Head>

      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/about.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center g_medium">About Eduland Immigration</h1>
          </div>
        </div>
      </section>

      <section className="mission_section common_padding pb-0">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 about_page_top">
              <p>
                Eduland immigration is a Chandigarh-based Visa consultancy firm
                in India that steps forward to facilitate aspiring students, job
                seekers, and individuals seeking a visitor Visa. Our insights
                are based on experience, knowledge, and data, and our
                organization is based on integrity and honesty. Eduland
                immigration is a registered and licensed firm owned and run by
                Mr. Gurpreet Singh Channa, who has a vast experience of over 15
                years in the consultancy world.
              </p>
              <p>
                We are passionate about providing authentic information and
                discussing all the pros and cons with our valued customers.
                Eduland assists its clients with application submission and
                helps bridge the gap between them and their desired Visa with
                exceptional expertise. We strive to ensure immigration-related
                objectives for all our customers in India.
              </p>
              <p>
                Being the leading immigration consultants in India, we offer
                tailored plans to individual clients based on their preferences
                and objectives. We are always keen to cover the extra mile to
                ensure our patrons’ productive outcomes and facilitate them by
                offering a highly convenient, hassle-free procedure.
              </p>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 mission_vision_left">
              <div className="mission_section_content">
                <div className="common_tittle text-left">
                  <h2 className="text-capitalise g_medium">Our Mission</h2>
                  <span>
                    <Image
                      width={60}
                      height={20}
                      src="/heading-line.svg"
                      alt="Icon"
                    />
                  </span>
                </div>
                <p>
                  We seek to be the trustworthy leaders in the consultancy field
                  in order to deliver brilliance to our kin and the precious
                  society. We aim to help aspiring people transform their dreams
                  and objectives into reality by assisting them in obtaining the
                  desired Visa.
                </p>
              </div>
            </div>

            <div className="col-lg-6 col-md-6 col-sm-12 mission_vision_right">
              <div className="mission_section_video">
                <video controls>
                  <source src="/video/video.mp4" type="video/mp4" />
                </video>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="mission_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 mission_vision_right">
              <div className="mission_section_video">
                <img src="/imgs/vision.jpg" alt="Visa consultancy story" />
              </div>
            </div>

            <div className="col-lg-6 col-md-6 col-sm-12 mission_vision_left">
              <div className="mission_section_content">
                <div className="common_tittle text-left">
                  <h2 className="text-capitalise g_medium">Our Vision</h2>
                  <span>
                    <Image
                      width={60}
                      height={20}
                      src="/heading-line.svg"
                      alt="Icon"
                    />
                  </span>
                </div>
                <p>
                  Being the leading immigration and student consultancy firm, we
                  endeavor to ensure a bright future by opening the doors of
                  opportunities to our valued clients. We are always passionate
                  about covering the extra mile for our valued clients and
                  constantly keep ourselves up-to-date to ensure authentic
                  information.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="testimonials_section common_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 common_tittle text-center">
              <p className="text-uppercase">CLIENTS TESTIMONIALS</p>
              <h2 className="text-capitalise g_medium">
                What Customers Saying about Eduland
              </h2>
              <span>
                <Image
                  width={60}
                  height={20}
                  src="/heading-line.svg"
                  alt="Icon"
                />
              </span>
            </div>

            <div className="col-md-12 testimonial_slider">
              <OwlCarousel
                className="service-carousel"
                responsiveClass="true"
                nav
                {...testimonial}
              >
                {testiData.map((item, index) => (
                  <div className="testi_slider_wrapper" key={index}>
                    <div className="row align_center">
                      <div className="col-md-3 col-sm-12 testi_slider_left text-center">
                        <Image
                          height={80}
                          width={80}
                          src="/new-icon/user.png"
                          alt="Visa consultancy story"
                        />
                        <h4 className="text-uppercase">{item.name}</h4>
                      </div>
                      <div className="col-md-9 col-sm-12 testi_slider_content text-left">
                        <Image
                          height={30}
                          width={30}
                          src="/comaa.png"
                          alt="Visa consultancy story"
                        />
                        <h4 className="text-uppercase">
                          {item.title}
                          <span>
                            <i className="bi bi-star-fill"></i>
                            <i className="bi bi-star-fill"></i>
                            <i className="bi bi-star-fill"></i>
                            <i className="bi bi-star-fill"></i>
                            <i className="bi bi-star-fill"></i>
                          </span>
                        </h4>
                        <p>{item.description}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </OwlCarousel>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
