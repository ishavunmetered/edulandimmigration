import Head from "next/head";
import Link from "next/link";
import Form from "../../components/Form";
function Service(data) {
  const service = data.service;
  const allServices = data.allservices;

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.png" />
        <title>{service.title} || Eduland Immigration</title>
        <meta name="description" content={service.title} />
        <meta property="og:title" content={service.title} />

        <meta
          property="og:url"
          content={`https://edulandimmigration.com/services/${service.slug}`}
        />
        <meta property="og:type" content="website" />
      </Head>
      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/inner-banner/education/ielts-banner.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center">{service.title}</h1>
          </div>
        </div>
      </section>

      <section className="services_single_section  common_padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4 col-sm-12 service_content_left">
              <div className="services_section_left w-100">
                <ul>
                  {allServices?.nodes.map((oneService) => (
                    <li key={oneService.slug}>
                      <Link href={`/services/${oneService.slug}`}>
                        <a>
                          {oneService.title}
                          <i className="bi bi-arrow-right"></i>
                        </a>
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
              <Form />
            </div>

            <div className="col-lg-9 col-md-8 col-sm-12 page_content service_content_right">
              <div className="services_content_image w-100">
                <img
                  src="/inner-banner/education/ielts-banner.jpg"
                  alt="Image"
                />
              </div>

              <div className="entry-content">
                <article
                  className="post_content"
                  dangerouslySetInnerHTML={{ __html: service.content }}
                ></article>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps(context) {
  const singleserviceRes = await fetch(
    "https://edulandimmigration.com/graphql",
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `
            query SingleService($id: ID!, $idType: ServiceIdType!) {
              service(id: $id, idType: $idType) {
                slug
                title
                content
              }
            }
              `,
        variables: {
          id: context.params.slug,
          idType: "SLUG",
        },
      }),
    }
  );

  const allservicesRes = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
        query Services {
          services {
            nodes {
              slug
              title
              content
            }
          }
       }`,
    }),
  });

  const singleServiceJson = await singleserviceRes.json();
  const allServiceJson = await allservicesRes.json();

  return {
    props: {
      service: singleServiceJson.data.service,
      allservices: allServiceJson.data.services,
    },
  };
}

export async function getStaticPaths() {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
        query Services {
          services {
            nodes {
              slug
              title
              content
            }
          }
       }`,
    }),
  });

  const json = await res.json();
  const services = json.data.services.nodes;

  const paths = services.map((service) => ({
    params: { slug: service.slug, services },
  }));

  return {
    paths,
    fallback: false,
  };
}

export default Service;
