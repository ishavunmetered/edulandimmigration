import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
export default function ConatctUs() {
  const initialValues = {
    name: "",
    email: "",
    phone: "",
    city: "",
    message: "",
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  };
  useEffect(() => {
    // console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      alert("Thanks For Contacting Us");
    }
  }, [formErrors, isSubmit]);

  const validate = (values) => {
    const errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    if (!values.name) {
      errors.name = "Name is required!";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!regex.test(values.email)) {
      errors.email = "This is not a valid email format!";
    }
    if (!values.phone) {
      errors.phone = "Phone is required!";
    }
    if (!values.city) {
      errors.city = "City is required!";
    }
    if (!values.message) {
      errors.message = "Message is required!";
    }
    return errors;
  };

  return (
    <>
      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/contact.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center g_medium">Contact Us</h1>
          </div>
        </div>
      </section>
      <section className="contact_page_Section common_padding">
        <div className="contact_address">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-sm-12 contact_address_column text-center">
                <div>
                  <Image width={40} height={40} src="/email.svg" alt="icon" />
                </div>
                <span>
                  <Image
                    width={80}
                    height={20}
                    src="/heading-line.svg"
                    alt="Icon"
                  />
                </span>
                <a href="mailto:contact@edulandimmigration.com">
                  contact@edulandimmigration.com
                </a>
              </div>

              <div className="col-md-4 col-sm-12 contact_address_column text-center">
                <div>
                  <Image width={40} height={40} src="/phone.svg" alt="icon" />
                </div>
                <span>
                  <Image
                    width={80}
                    height={20}
                    src="/heading-line.svg"
                    alt="Icon"
                  />
                </span>
                <a href="tel:+91 99155-62155">+91-99155-62155</a>
              </div>

              <div className="col-md-4 col-sm-12 contact_address_column text-center">
                <div>
                  <Image
                    width={40}
                    height={40}
                    src="/location.svg"
                    alt="icon"
                  />
                </div>
                <span>
                  <Image
                    width={80}
                    height={20}
                    src="/heading-line.svg"
                    alt="Icon"
                  />
                </span>
                <p>
                  <Link href="https://goo.gl/maps/NoBYzgdrcVKHu9nJ9">
                    <a target="_blank" rel="noopener noreferrer">
                      First Floor, SCO 2455-2456, Sector 22 C, Chandigarh,
                      160022
                    </a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="contact_form common_padding">
          <div className="container">
            <div className="row">
              <div className="col-md-12 common_tittle text-center">
                <h2 className="text-capitalise g_medium">
                  Contact Us / Register
                </h2>
                <span>
                  <Image
                    width={80}
                    height={30}
                    src="/heading-line.svg"
                    alt="Icon"
                  />
                </span>
              </div>

              <div className="col-md-12 form_contact">
                {Object.keys(formErrors).length === 0 && isSubmit ? (
                  <div className="alert alert-success" role="alert">
                    Thank You For Contacting us
                  </div>
                ) : (
                  console.log("")
                  //   <div className="alert alert-danger" role="alert">
                  //   Invalid Response
                  // </div>
                )}
                <form className="row" onSubmit={handleSubmit}>
                  <div className="col-md-4 form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="name"
                      onChange={handleChange}
                      value={formValues.name}
                      placeholder="You Name*"
                    />
                    <p className="error-field">{formErrors.name}</p>
                  </div>
                  <div className="form-group col-md-4">
                    <input
                      type="email"
                      className="form-control"
                      name="email"
                      value={formValues.email}
                      onChange={handleChange}
                      placeholder="Your Email*"
                    />
                    <p className="error-field">{formErrors.email}</p>
                  </div>
                  <div className="form-group col-md-4">
                    <input
                      type="number"
                      className="form-control"
                      name="phone"
                      value={formValues.phone}
                      onChange={handleChange}
                      placeholder="Phone Number*"
                    />
                    <p className="error-field">{formErrors.phone}</p>
                  </div>
                  <div className="form-group col-md-12">
                    <input
                      type="text"
                      className="form-control"
                      name="city"
                      value={formValues.city}
                      onChange={handleChange}
                      placeholder="City You'r Living In*"
                    />
                    <p className="error-field">{formErrors.city}</p>
                  </div>
                  <div className="form-group col-md-12">
                    <textarea
                      name="message"
                      rows="4"
                      cols="50"
                      value={formValues.message}
                      onChange={handleChange}
                      className="form-control"
                      placeholder="Your Message"
                    ></textarea>
                    <p className="error-field">{formErrors.message}</p>
                  </div>

                  <div className="col-md-12 form-group mb-0 text-center">
                    <button type="submit" className="common_btn common_btn2">
                      SUBMIT
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div className="contact_map ">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3429.5940942494917!2d76.76717541548304!3d30.72980959282382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390fed1331471f85%3A0x142af674791714ed!2sEduland%20Immigration!5e0!3m2!1sen!2sin!4v1648540648631!5m2!1sen!2sin"
            referrerPolicy="no-referrer-when-downgrade"
          ></iframe>
        </div>
      </section>
    </>
  );
}
