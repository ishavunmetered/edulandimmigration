import Head from "next/head";
import Form from "../../components/Form";
function Blog(data) {
  const post = data.post;

  return (
    <>
      <Head>
        <title>{post.title} - Eduland Immigration</title>
        <meta name="description" content={`Learn more about ${post.title}`} />
        <meta
          property="og:title"
          content={`${post.title} - Eduland Immigration`}
        />
        <meta
          property="og:description"
          content={`Learn more about ${post.title}`}
        />
        <meta
          property="og:url"
          content={`https://edulandimmigration.com/blogs/${post.slug}`}
        />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/inner-banner/education/ielts-banner.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center g_medium">{post.title}</h1>
          </div>
        </div>
      </section>

      <section className="services_single_section  common_padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4 col-sm-12 service_content_left">
              <Form />
            </div>

            <div className="col-lg-9 col-md-8 col-sm-12 page_content service_content_right">
              <div className="services_content_image w-100">
                <img
                  src="/inner-banner/education/ielts-banner.jpg"
                  alt="Image"
                />
              </div>

              <div className="entry-content">
                <article
                  className="post_content"
                  dangerouslySetInnerHTML={{ __html: post.content }}
                ></article>
              </div>

              <div className="video_block">
                <iframe
                  width=""
                  height=""
                  src="https://www.youtube.com/embed/ooFICI8ADgQ"
                  title="YouTube video player"
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps(context) {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: `
                query SinglePost($id: ID!, $idType: PostIdType!) {
                    post(id: $id, idType: $idType) {
                        title   
                        slug
                        content
                        featuredImage {
                            node {
                                sourceUrl
                            }
                        }
                    }
                }
            `,
      variables: {
        id: context.params.slug,
        idType: "SLUG",
      },
    }),
  });

  const json = await res.json();

  return {
    props: {
      post: json.data.post,
    },
  };
}

export async function getStaticPaths() {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
        query AllPostsQuery {
            posts {
                nodes {
                    slug
                    content
                    title
                    featuredImage {
                        node {
                            sourceUrl
                        }
                    }
                }
            }
        }`,
    }),
  });

  const json = await res.json();
  const posts = json.data.posts.nodes;

  const paths = posts.map((post) => ({
    params: { slug: post.slug },
  }));

  return {
    paths,
    fallback: false,
  };
}

export default Blog;
