import React from "react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

export default function Blog({ posts }) {

  return (
    <>
      <Head>
        <title>Blogs || Eduland Immigration</title>
        <meta property="og:title" content="Blogs || Eduland Immigration" />
        <meta property="og:type" content="website" />
        <meta
          property="og:description"
          content="Learn more about Eduland Immigration Blogs"
        />
        <meta
          property="og:url"
          content="https://edulandimmigration.com/blogs"
        />
        <meta property="og:type" content="website" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <section
        className="sngle_banner"
        style={{
          backgroundImage: 'url("/about-banner.jpg")',
        }}
      >
        <div className="banner_content_inner">
          <div className="container">
            <h1 className="text-center g_medium">Blogs</h1>
          </div>
        </div>
      </section>

      <section className="blogs_section common_padding grey_bg">
        <div className="container">
          {/* <div className="col-md-12 service_slider_main"> */}
          <div className="row">
            {posts.nodes.map((post) => (
              <div className="col-md-4 col-sm-12 blog_main" key={post.slug}>
                <div className="blog_slider_column text-center">
                  <Link href={`/blogs/${post.slug}`}>
                    <a>
                      <div className="blog_slider_image">
                        <img
                          src={post.featuredImage.node.sourceUrl}
                          alt={post.slug}
                        />
                      </div>
                      <div className="blog_slider_content  text-left">
                        <h4>{post.title}</h4>
                        <div className="date_time">
                          <Image
                            width={20}
                            height={20}
                            src="/clock.svg"
                            alt="Icon"
                          />
                          <span>{post.date}</span>
                        </div>
                        {/* <p>{blog.description}</p> */}
                        <div className="common_btn common_btn2">
                          Read More<i className="bi bi-arrow-right"></i>
                        </div>
                      </div>
                    </a>
                  </Link>
                </div>
              </div>
            ))}
            {/* </div> */}
          </div>
        </div>
      </section>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch("https://edulandimmigration.com/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: ` 
      query AllPostsQuery {
        posts {
          nodes {
            slug
            title
            date
            featuredImage {
              node {
                sourceUrl
              }
            }
          }
        }
      }`,
    }),
  });
  const json = await res.json();

  return {
    props: {
      posts: json.data.posts,
    },
  };
}
